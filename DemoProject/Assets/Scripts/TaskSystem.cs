﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskSystem : MonoBehaviour
{
    public static int taskCompletion;
    private int taskCompletionMax;
    [SerializeField] Image taskCompletionImage;
    

    public void Start()
    {
        this.taskCompletionMax = 50;
        taskCompletion = 0;
        TaskImageUpdate();
    }

    //gets value of 
    public int GetTaskCompletion()
    {
        return taskCompletion;
    }

    public float GetTaskCompletionPercent()
    {
        return (float)taskCompletion / taskCompletionMax;
    }
   
    public void FillTask(int taskFillAmount)
    {
        taskCompletion += taskFillAmount;
        if (taskCompletion > taskCompletionMax) taskCompletion = taskCompletionMax;
        TaskImageUpdate();

    }

    private void TaskImageUpdate()
    {
        taskCompletionImage.fillAmount = (float)taskCompletion / taskCompletionMax;
        
    }

}


