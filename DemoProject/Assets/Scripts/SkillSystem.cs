﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillSystem : MonoBehaviour
{
    // public static int charSkillCheck;
    public static int skillFail;
    private int skillMax;
    //[SerializeField]
    public int buildSkill;
    //[SerializeField]
    public int collectSkill;
    //[SerializeField]
    public int craftSkill;
    //[SerializeField]
    public int brewSkill;




    public void CompareSkill()
    {
        skillMax = Random.Range(-1, 4);
        if (transform.parent != null && transform.parent.tag == "BuildSlot")
        {
            skillFail = skillMax - buildSkill;
            if (skillFail == 0)
            {

                skillFail = 1;

            }
            Debug.Log("Build fail is: " + skillFail);
        }
        if (transform.parent != null && transform.parent.tag == "CollectSlot")
        {
            skillFail = skillMax - collectSkill;
            if (skillFail == 0)
            {

                skillFail = 1;

            }
            Debug.Log("Collect fail is: " + skillFail);
        }
        if (transform.parent != null && transform.parent.tag == "CraftSlot")
        {
            skillFail = skillMax - craftSkill;
            if (skillFail == 0)
            {

                skillFail = 1;

            }
            Debug.Log("Craft fail is: " + skillFail);
        }
        if (transform.parent != null && transform.parent.tag == "BrewSlot")
        {
            skillFail = skillMax - brewSkill;
            if (skillFail == 0)
            {

                skillFail = 1;

            }
            Debug.Log("Brew fail is: " + skillFail);
        }
        if (transform.parent != null && transform.parent.tag == "RestSlot")
        {

            skillFail = 0;
            Debug.Log("Rest slot is reset");
        }
    }

}  

    

