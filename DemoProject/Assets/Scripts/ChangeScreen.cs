﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScreen : MonoBehaviour
{

    public GameObject Book;
    public GameObject Map;
    public GameObject Room;

    // Start is called before the first frame update
    void Start()
    {

        Book.SetActive(false);
        Map.SetActive(false);
        Room.SetActive(true);

    }

    // Update is called once per frame

}
