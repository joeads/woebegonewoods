﻿using UnityEngine;
using UnityEngine.UI;

public class HappinessSystem : MonoBehaviour
{
    public static int happiness;
    private int happinessMax;
    [SerializeField] Image happinessImage;

    public void Start()
    {
        this.happinessMax = 50;
        happiness = happinessMax;
        ImageUpdate();
    }
//gets value of happiness state
    public int GetHappiness()
    {
        return happiness;
    }

    public float GetHappinessPercent()
    {
        return (float)happiness / happinessMax;
    }
//decreases happiness 
    public void Decrease(int decreaseAmount)
    {
        happiness -= decreaseAmount;
        if (happiness < 0)
        {

            happiness = 0;

        }
        if (happiness > happinessMax)
        {

            happiness = happinessMax;

        }
        ImageUpdate();
    }
//increases happiness
    //public void Increase(int increaseAmount)
    //{
    //    happiness += increaseAmount;
    //    if (happiness > happinessMax)
    //    {

    //        happiness = happinessMax;

    //    }
        
    //    ImageUpdate();
    //}

    private void ImageUpdate()
    {
        happinessImage.fillAmount = (float) happiness / happinessMax;
    }
    
}
