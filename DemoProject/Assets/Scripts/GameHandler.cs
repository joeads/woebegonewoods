﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHandler : MonoBehaviour
{
    public List<Slots> slot;
    public List<MarkScript> mark;
    public List<SkillSystem> skill;
    public int fail;




    public void FailValue()
    {
        foreach (SkillSystem currentFail in skill)
        {
            currentFail.CompareSkill();

        }
    }

    

    public void TaskOnClick()

    {
        
        
        Statuscheck();

        if(SubmitCounterScript.submitCounter != 20)
        {

            foreach (Slots currentSlot in slot)
            {
                FailValue();

                currentSlot.ActivateSlots();


            }
            SubmitCounterScript.submitCounter += 1;

        }
        else
        {

            return;

        }
        
        
        

    }

    

    public void Statuscheck()
    {
        
        foreach(MarkScript currentmark in mark)
        {
            
            currentmark.RunCheck();

        }

    }

    


}
