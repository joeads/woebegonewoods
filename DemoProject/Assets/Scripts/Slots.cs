﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;



public class Slots : MonoBehaviour, IDropHandler
//allows character blocks to snap into place in all slots
{
    [SerializeField]
    private int happinessCount;
    private static int boolint;
    [SerializeField]
    TaskSystem taskSystem;
    public GameObject item
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }

            return null;
        }
    }

    public GameObject check
    {

        get
        {

            if (transform.childCount > 0)
            {

                return transform.GetChild(0).GetChild(3).GetChild(1).gameObject;

            }

            return null;
        }

    }

    public GameObject xmark
    {

        get
        {

            if (transform.childCount > 0)
            {

                return transform.GetChild(0).GetChild(3).GetChild(0).gameObject;

            }

            return null;
        }

    }

    public void OnDrop(PointerEventData eventData)
    {

        if (!item)
        {
            DragHandler.item.transform.SetParent(transform);
        }


    }

    public void ActivateSlots()
    {

        if (!item)
        {
            return;
        }

       

        SkillSystem skillValue = item.GetComponent<SkillSystem>();
        skillValue.CompareSkill();

        HappinessSystem happinessValue = item.GetComponent<HappinessSystem>();
        happinessValue.Decrease(happinessCount);


        boolint = SkillSystem.skillFail;
        
        Debug.Log("Test: " + boolint);

        if (HappinessSystem.happiness <= 0)
        {

            return;

        }
        else
        {

            if (boolint < 0)
            {

                xmark.SetActive(true);

            }
            if (boolint > 0)
            {


                taskSystem.FillTask(3 * boolint);
                check.SetActive(true);

            }

        }
        
        


        

        MarkScript runnumbers = item.GetComponent<MarkScript>();

        
        

    }

    
}

